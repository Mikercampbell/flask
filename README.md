# README #

A simple demonstration of Flask.

### How do I get set up? ###

* First, make sure you have Flask installed (pip install Flask..)

####Linux / OS X####

* Run `export FLASK_APP=server.py`
* Then `flask run`
* The app will be on http://localhost:5000

####Windows####

* Run `set FLASK_APP=server.py`
* Then `flask run`
* The app will be on http://localhost:5000

### Who do I talk to with questions? ###

* Repo Admin (Mike Campbell)