from flask import Flask, render_template
from math import sqrt

app = Flask(__name__)

# My index page.. not much to see here
@app.route('/')
def hello_world():
    return 'Hello World! Welcome to my Flask App!'

# Take in a url parameter and return it in the page
@app.route('/hello/<name>')
def hello(name):
    return 'Hey, %s, how are ya!?' % name


# take in two casted parameters and add them before returning them
@app.route('/add/<int:num1>+<int:num2>')
def add(num1, num2):
    sum = num1 + num2
    return 'The Sum of %d and %d is %d' % (num1, num2, sum)

# take in a casted parameter and pass it to a template for rendering
@app.route('/getsqrt/<int:myNum>')
def page(myNum):
    mySqrtNum = sqrt(myNum)
    return render_template('sqrt.html', num=myNum, sqrtnum=mySqrtNum )

# handle 404 errors!
@app.errorhandler(404)
def page_not_found(error):
    return render_template('page_not_found.html'), 404

